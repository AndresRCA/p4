## PIC1 de transmision

Se encarga de:

1. Transmitir el byte de estado de operacion (que operacion se hara con la siguiente transmision).
2. Transmitir el bit de paridad, 1 es que se esta mandando el byte de estado y 0 es por ejemplo el ADRESH.
3. Detectar la interrupcion de RB0 y tomar medidas de transmision.
4. Detectar la interrupcion de A/D y tomar medidas de transmision.
5. Detectar por pooling el boton de reseteo.

---

## PIC2 de recepcion

Se encarga de:

1. Actuar como un temporizador usando el TMR1 con interrupcion (igual que el proyecto pasado).
2. Mostrar los digitos en los displays.
3. Recibir los datos de transmision (el bit de paridad tambien) y verificar condiciones para determinar la accion a tomar.
4. En el caso de recibir un mensaje de estado, guardar ese valor a una variable para su uso posterior para la determinacion de la accion a tomar del siguiente mensaje recibido.
5. Priorizar la conversion A/D cuando se reciba un mensaje (primero verificar si hay que convertir).

---

Notas: 

* El PIC2 solo tendra la interrupcion del TMR1 y la de recepcion (si se decide hacerlo por interrupcion), las otras interrupciones las maneja el PIC1