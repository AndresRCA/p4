list P = 16F877A
include <P16F877A.inc>

isStopped EQU 20h ; cuando es 0 es falso, 1 es verdadero (en el bit 0)
isStarting EQU 21h ; cuando es 0 el conteo no ha comenzado, 1 es que ya empezo (en el bit 0)
retraso EQU 22h
;este es status es simbolico, realmente no se utiliza, pero en el pic2 si.
;Nota: el bit de paridad cuando es 1 significa RB0 o RD2, si es 0 significa que el valor que se transmitio es el ADRESH
status EQU 23h ; bit 1 = 1 -> PORTD, 2 (se presiono el puerto RD2), bit 0 = 1 -> RB0 (se presiono RB0)

org 0h
        goto Fig
org 4h
;*****************************************
		btfss PIR1, 6
		goto RB
		call ADInt
		retfie
RB		btfsc isStarting, 0 ; el pic2 deberia hacer esto mismo
		goto Next
		call Load1 ; cargo 1 en el bit de paridad para indicar que se presiono RB0 en el estado
		movlw b'00000001' ; se presiono RB0 por la primera vez
		movwf TXREG ; el programa del pic2 se preocupara de que hacer con su primera interrupcion con RB0
		bsf isStarting, 0 ; el pic2 deberia hacer esto mismo
		bcf INTCON, 1 ; bajo la bandera de RB0
		retfie
Next    call RB0Int ; si la bandera es 1 ira aqui
		retfie
;*****************************************
org 40h
;*********** Configuracion ***************
Fig    	clrf isStopped
		clrf isStarting
        bsf STATUS, 5
		; el pin pulsador es PORTD, 2
        bsf INTCON, 7 ; global interrupt
        bsf INTCON, 4 ; external rb0 interrupt
		bsf INTCON, 6 ; habilito interrupciones en perifericos
		bsf PIE1, 6 ; habilito la interrupcion del conversor A/D
		; el canal por defecto es AN0, y es el que se va a utilizar, el pin de master clear que esta arriba de AN0 servira como la fuente de 5V del potenciometro mientras que AN1 que esta debajo sera la tierra
        ; la velocidad de conversion sera Fosc/2, el cual es por defecto, tal vez deberia ponerlo mas lento para que la transmision fluya mejor, en recepcion lo primero a preguntar deberia ser la conversion
		; por defecto el resultado ya esta justificado a la izquierda
		bcf TRISC, 6
		bsf TXSTA, 6 ; habilito el noveno bit
		bsf TXSTA, 2 ; coloco la transmision en alta velocidad
		movlw d'103' ; BAUD RATE (K) de 19.2 con Fosc = 4MHz
		movwf SPBRG
		bsf TXSTA, 5 ; habilito transmisiones
        bcf STATUS, 5
		bsf ADCON0, 0 ; enciendo el modulo conversor
		bsf RCSTA, 7 ; SPEN = 1 (Serial Port Enable bit)
;*****************************************
Eh		btfss isStarting, 0
		goto Eh
Main	btfss PORTD, 2 ; pregunto si el reset esta en 1
        goto Main
		btfss isStopped, 0 ; si PORTD,2 esta en 1 y tambien esta detenido borro los valores y activo el conversor
		goto Main
		call Load1
		movlw b'00000010' ; bit 1 = 1, RD0 se presiono y esta detenido el temporizador, reseteo el contador del pic2
		movwf TXREG
;****** Estado de conversion (isStopped, 0 = 1  && PORTD, 2 = 1 (previamente)) = true ***********
Convert	btfss isStopped, 0 ; antes de activar el conversor pregunto si sigue detenido, si ya no esta detenido se va al Main donde el programa solo se encarga de mostrar el numero y el tmr1 esta activado
		goto Main
		; no creo que haya que transmitir b'00000100'... con solo saber que el bit de paridad es 0 el pic2 deberia saber que el valor es para convertir
		bsf ADCON0, 2
		call Delay ; aqui podria poner un retardo para no bombardear al pic2 cada vez que ocurra una interrupcion de conversion en el pic1 mientras el pic2 no ha terminado de sacar los digitos de la conversion previa
		goto Convert
;************************************************************************************************

;********Funciones que ocurren durante 1 segundo*******
;*****************
;retardo para darle tiempo al algoritmo de conversion del pic2 cuando reciba la transmision   
Delay   movlw d'255'
        movwf retraso ; espero que retraso no cargue algun valor raro si la conversion llega a interrumpir antes de esta linea, o justamente en esta linea
Back    decfsz retraso, 1
		goto Back
        return
;*****************

;*************************************************************
;*****************
; funcion que carga 1 en el bit de paridad
Load1	bsf STATUS, 5
		bsf TXSTA, 0
		bcf STATUS, 5
		return
;*****************

;*****************
; funcion que carga 0 en el bit de paridad
Load0	bsf STATUS, 5
		bcf TXSTA, 0
		bcf STATUS, 5
		return
;*****************

;*****************
; funcion que espera que la transmision previa se termine, en caso de que sea necesario
WaitTX	bsf STATUS, 5
Full	btfss TXSTA, 1
		goto Full
		bcf STATUS, 5
		return
;*****************

;*****************
; rutina que se usa al llamar la interrupcion del RB0 		
RB0Int	bcf INTCON, 1 ; apago la bandera de RB0
		call Load1
		movlw b'00000001' ; bit 0 = 1, RBO se presiono
		movwf TXREG ; solo necesito mandar el estado, no hay valor de operacion como el ADRESH
		btfss isStopped, 0 ; pregunto si esta detenido, esto es importante para entrar al bucle de reseteo
		goto Stop
		bcf isStopped, 0 ; cuando lo resumo lo pongo en 0
		return
Stop	bsf isStopped, 0 ; cuando lo paro lo pongo en 1
		return
;*****************

;*****************
; rutina que se usa al llamar la interrupcion del A/D		
ADInt	bcf PIR1, 6 ; apago la bandera de A/D
		movf ADRESH, 0
		; call WaitTX; tal vez tenga que preguntar si TXREG esta vacio o no
		call Load0 ; cargo 0 en el bit de paridad para indicar que se va a mandar un valor de operacion
		movwf TXREG ; transmito el valor de ADRESH
		return
;*****************
;************************************************************************************************
end
